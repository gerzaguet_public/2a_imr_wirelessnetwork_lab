% --- ENSSAT_OFDM.m 
% --- 
% Description 
%	  Script for OFDM signal generation and decoding 
%	  User parameters are defined at the beginning of the script 
%	  Some parts have to be completed. See lab subject
% --- 
% Syntax 
%     ENSSAT_OFDM
% ---
% Variables 
%
% --- 
% v 1.0 - Robin Gerzaguet.


% % --- Clear all stuff 
clear all; 
close all;
clc; 

addpath('./path');



% ---------------------------------------------------- 
%% --- User parameters  
% ---------------------------------------------------- 
% --- Transmission parameters
sizeConstellation	= 4;					% --- Constellation size [4 : QPSK, 16 : 16-QAM]

% --- OFDM parameters
sizeFFT				= ;					% --- Number of OFDM subcarriers  (i.e FFT size in samples)
typePrefixe		= 'CP';					% --- Type of Cyclic prefix used 
													    % --- 'none'	: No prefix used 
													    % --- 'guard'	: Guard intervall
													    % --- 'cp'		: Cyclic prefix 
													    % --- 'cs'		: Cyclic suffix
sizePrefix			= ;				% --- Size of prefix (for none, it is automatically overwritten to 0)
% --- Channel parameters
freq            = ;           % --- LTE  sampling frequency (in Hz)
channelTap			= [1];        % --- Channel tap coefficients
snrChannel			= 90;					% --- Signal to Noise ratio (dB)
% --- Additionnal Parameters
nbOFDMSymbols		= 140;				% --- Number of OFDM symbols
overSamp			= 1; 					  % --- Oversampling factor for RF stage (1 for OFDM)
subcarriers			= allocateLTE(sizeFFT);                  % --- Allocated subcarriers
													%	1:sizeFFT	: All subcarriers allocated
													%	[3]			: Only subcarrier 3 is enable 
													% allocateLTE(sizeFFT): returns the default Broadband LTE allocation

% ---------------------------------------------------- 
%% --- Tx Stage
% ----------------------------------------------------                                                
tx_stage;

% ---------------------------------------------------- 
%% --- Channel model 
% ----------------------------------------------------    
channel;

% ---------------------------------------------------- 
%% --- Receiver
% ----------------------------------------------------                                                



                                                   





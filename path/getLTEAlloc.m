% --- getLTEAlloc.m 
% --- 
% Description 
%    Returns Ressource block allocation for a given FFT size, associated to LTE parameters.
%	  For Example, if FFT size is set to 1024, 50 RB will be allocated, in both part of Spectrum 
%	  The outputs are 2 cell structure, block shift and RB vector, that should be feed into simRF structure 
% --- 
% Syntax 
%	  [blockShift,vectRB,nbRB,nCP,occupiedCarriers]	= getLTEAlloc(fftSize)
%							% --- Input parameters 
%								  fftSize	  : Size of FFT. Should belongs to [128-2048]
%							% --- Output parameters 
%								  blockShift  : Shift in frequency
%								  vectRB      : Vector of allocated RB. Associated to shift in frequencies 
%                                 nbRB        : Number of allocated RB 
%                                 nCp         : CP length
%								  occupiedCarriers	: Vector of occupied carriers
%		OR 
%	[struc]		= getLTEAlloc(struc)
%							% --- Input parameters 
%								  struc		  : simRF structure for modulation (should contain fftSize)
%							% --- Output parameters 
%								  struc		  : Updating structure (with blockShift, vectRB and sizeRB field)
% --- 
% v 1.0 - Robin Gerzaguet.

function [varargout]	= getLTEAlloc(param)
	% ---------------------------------------------------- 
	%% --- Allocation depends on entry parameter 
	% ---------------------------------------------------- 
	if isstruct(param)
		% --- Entry parameter is simRF structure 
		fftSize				= param.nFFT;
		cntType				= 1;
	else 
		% --- Entry parameter is the FFT size 
		fftSize				= param;
		cntType				= 0;
	end
	% --- Checking that FFT belongs to LTE modes 
	if isempty(find(fftSize==[2.^(7:12) 1536], 1));
		error('simRF:getLTEAlloc','Impossible allocation, FFT size should belong to LTE use case');
	end
	% --- Switch on LTE use case 
	switch fftSize 
		case 128
			blockShift		= [1;92];
			vectRB			= {(1:3),(1:3)};
			nCp             = 9;
		case 256 
			blockShift		= [1;160];
			vectRB			= {(1:7),(1:8)};
			nCp             = 18;
		case 512
			blockShift		= [1;356];
			vectRB			= {(1:12),(1:13)};
			nCp             = 36;
		case 1024 
			% --- 1024 use-case
			blockShift		= [1;724];
			vectRB			= {(1:25),(1:25)};
			nCp             = 72;
		case 1536
			blockShift		= [1;1080];
			vectRB			= {(1:37),(1:38)};
			nCp             = 108;
		case 2048 
			blockShift		= [1;1448];
			vectRB			= {(1:50),(1:50)};
			nCp             = 144;
        case 4096 % extented as LTE as no 4096 mode
			blockShift		= [1;2896];
			vectRB			= {(1:100),(1:100)};
			nCp             = 288;           
	end 
	% --- Getting number of RB
	nbRB    = length(cell2mat(vectRB));
	% --- Getting occupied carrier vector 
	occupiedCarriers  = zeros(1,nbRB*12);
	cnt				  = 1;
	for i = 1 : 1 : length(blockShift)
		currRB = vectRB{i};
		for i2 = 1 : 1 : length(currRB)
			% --- Current data block of carriers 
			currCarr		   = 1+(currRB(i2)-1)*12:currRB(i2)*12;
			% ---From current data block, go to carriers
			occupiedCarriers(1+(cnt-1)*12:cnt*12) = blockShift(i) +  currCarr;
			cnt				  = cnt + 1;
		end
	end
	if cntType == 1 
		% --- Output is updated structure 
		param.blockShift	  = blockShift;
		param.vectRB		  = vectRB;
        param.sizeRB          = 12;
		param.nCp			  = nCp;
		param.occupiedCarriers  = occupiedCarriers;
		param.nbCarriers	  = length(occupiedCarriers);
		varargout			  = {param};
	else 
		% --- Output is cell of output parameters 
		varargout			  = {blockShift,vectRB,nbRB,nCp,occupiedCarriers};
	end
end







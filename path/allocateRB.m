% --- allocateRB.m 
% --- 
% Description 
%    simRF kernel function.
%	Allocate Frequency element to appropriate configuration for multicarrier modulation 
%	  The allocated RB are set and the potential carrier shift. The output is a vector of index of data carrier between 1 and nFFT and the vector of size nbRB x sizeRB of allocated carrier 
% --- 
% Syntax 
%			[occupiedCarriers,carrierRb]  = allocateRB(vectRB,blockShift,sizeRB,nFFT)
%							% --- Input parameters 
%									  vectRB	  : cell of enabled group of RB
%									  blockShift  : vector of Frequency shifting for enabled carriers. Its lenght must be equal to the cell element of vectRB and each group of RB will be shifting with the appropriate value of blockShift
%									  sizeRB	  : RB size in carrier
%                                     nFFT        : FFT size
%							% --- Output parameters 
%									  occupiedCarriers : vector of occupied carriers 
%									  carrierRb	  : Matrix opf enable carrier of size NbRB x sizeRB 
% --- 
% v 1.0 - Robin Gerzaguet

function [varargout]  = allocateRB(varargin) 
	if nargin == 1
		% Structure has be given 
		config				  = varargin{1};
		vectRB				  = config.vectRB;
		blockShift			  = config.blockShift;
		sizeRB				  = config.sizeRB;
		nFFT				  = config.nFFT;
		outStruc			  = 1;
	else 
		% Each parameter is used 
		vectRB		  = varargin{1};
		blockShift	  = varargin{2};
		sizeRB		  = varargin{3};
		nFFT		  = varargin{4};
		outStruc			  = 0;
	end

	if ~iscell(vectRB)
		% --- Direct transformation
		carrierRb			= ones(length(vectRB),sizeRB);
		carrierRb(:,1)		= 1+(vectRB.'-1)*sizeRB;
		carrierRb			= blockShift +cumsum(carrierRb,2).';
		occupiedCarriers	= carrierRb(:);
	else 
		% --- Each RB has a shift 
		if numel(blockShift) ~= size(vectRB,2)
			error('simRF:allocateRB','Unable to perform carrier mapping, vectRB should be a vector or matrix with same size as blockShift');
		else 
			carrierRb		= [];
			for iN = 1 : 1 :  size(vectRB,2)
				currRB				= vectRB{iN};
				currBS				= blockShift(iN);
				carrierRbTmp		= ones(length(currRB),sizeRB);
				carrierRbTmp(:,1)	= 1+(currRB.'-1)*sizeRB;
				carrierRbTmp		= currBS +cumsum(carrierRbTmp,2).';
				carrierRb			= [carrierRb carrierRbTmp];
			end
			occupiedCarriers	= carrierRb(:);
		end
	end
	% --- Checking that frequency mapping is possible 
	if sum(occupiedCarriers > nFFT) > 0
		error('simRF:allocateRB','Frequency mapping impossible : try to allocate out of band carriers');
	end
	if outStruc 
		config.occupiedCarriers	  = occupiedCarriers;
		config.nbCarriers		  = length(occupiedCarriers);
		config.carrierRb		  = carrierRb;
		varargout	= {config};
	else 
		varargout = {occupiedCarriers,carrierRb};
	end
end	

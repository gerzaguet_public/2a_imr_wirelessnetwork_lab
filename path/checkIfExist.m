% --- checkIfExist.m
% --- 
% Description
%		src function. Controls the existence of a givent 'field' in a 
%		'structure' and eventually initialize it to 'value' if it does not
%		exist.
%		value is the value of the field overwritted
% ---
% Syntax
%		[structure bool value]=checkIfExist(structure,field,[value])
%	Examples : config
%						config =
%								field_1 : 0
%								field_2 : 2
%
%			% ----------------------------------------------------
%			[config bool]=checkIfExist(structure,'field_1')
%						config 	= 
%								field_1 : 0
%								field_2 : 2
%						bool  	= 1
%			field_1 exists. Nothing is changed and bool returns 1.
%			% ----------------------------------------------------
%			[config bool]=checkIfExist(structure,'field_2',12)
%						config 	= 
%								field_1 : 0
%								field_2 : 2
%						bool  	= 1
%			field_2 exists, so the value is not modified, bool returns 1
%			% ----------------------------------------------------
%			[config bool]=checkIfExist(structure,'field_3')
%						config 	= 
%								field_1	: 0
%								field_2 : 2
%						bool  	= 0
%			field_3 does not exist, bool returns false, and nothing is changed
%			% ----------------------------------------------------
%			[config bool]=checkIfExist(structure,'field_3','12')
%						config 	= 
%								field_1	: 0
%								field_2	: 2
%								field_3 : 12
%						bool  	= 0
%			field_3 is created and initiate to '12', bool returns false.
%			% ----------------------------------------------------
% ---
% v 2.0 - Jan. 2013 - Robin Gerzaguet.


function [params,bool,value]=checkIfExist(varargin)

% --- Initial checks
narginchk(1, 4);

% --- Type check
if ~ischar(varargin{2})
    error('simRF:argChk', 'Argument field must be a char');
end
if  nargin==3 && ~ischar(varargin{3})
    error('simRF:argChk', 'Argument value must be a char');
end

% --- Getting parameters
params=varargin{1};
field=varargin{2};
value = 0;
% if nargin == 3
%     value = eval(varargin{3});
% end
str=['params.' field];

% --- setting parameters
bool = false;
try
    ev = eval([str ';']);
    if nargin == 3
        value = ev;
    end
        eval([str ';']);
    bool=true;
catch exception
    if strcmp(exception.identifier,'MATLAB:nonExistentField')
        bool=false;
    end
    if nargin==3
        str2=[str '=' varargin{3} ';'];
        eval(str2);
        value = eval(varargin{3});
    end
end




% --- plotTFGrid.m 
% --- 
% Description 
%    Plotting the time frequency grid of a OFDM burst 
% --- 
% Syntax 
%    plotTFGrid(qamMat);
%			  % --- Input parameters 
%					qamMat	= Time frequency grid 
% --- 
% v 1.0 - Robin Gerzaguet.

function plotTFGrid(qamMat)
	% --- Prepare Figure 
  figure;
  % --- Time frequency computation 
  TF	= fftshift(10*log10(abs(qamMat)));
  % --- Getting frequency indexes 
  nbF	= size(qamMat,2);
  xData	= ((0:1:nbF-1)/nbF)-0.5;
  % --- Plotting the system 
  imagesc(TF,'XData',xData);
  hold on; 
  xlabel('Subcarrier');
  ylabel('Time index');
end

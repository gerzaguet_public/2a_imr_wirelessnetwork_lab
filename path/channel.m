% --- channel.m 
% --- 
% Description 
%	  Multipath channel model 
% --- 
% Syntax 
%     channel
%
% --- 
% v 1.0 - Robin Gerzaguet.


% ---------------------------------------------------- 
%% --- Channel model
% ---------------------------------------------------- 
if isa(channelTap,'char')
    % Seed 
%    rng('default');
%    rng(11022018);
    % Switch on channel model 
    switch lower(channelTap)
        case 'etu'
            % --- Getting delays and power delay profile
            strucChan.powerDelay	= [-1.0 -1.0 -1.0 +0.0 +0.0 +0.0 -3.0 -5.0 -7.0];
            strucChan.tapDelay		= [0   50  120  200  230  500 1600  2300 5000] * 1e-9;
        case 'eva'
            strucChan.powerDelay	= [0.0 -1.5 -1.4 -3.6 -0.6 -9.1 -7.0 -12.0 -16.9];
            strucChan.tapDelay		= [0 30 150 310 370 710 1090 1730 2510] * 1e-9;
        otherwise
            error('IMR:lab','Unknown channel model')
    end
    % --- Forcing delay on grid
    tapDelayD   = ((1+round(strucChan.tapDelay*freq))/freq);
    % --- Average CIR
    channelTap = zeros(1,floor(tapDelayD(end)*freq));
    channelTap(floor(tapDelayD*freq))	=  channelTap(floor(tapDelayD*freq))+10.^(strucChan.powerDelay/10);
    % Adding rayleigh law 
    rayLaw  = 1/sqrt(2) * (randn(1,length(channelTap)) + 1i*randn(1,length(channelTap)));
    % Channel output 
    channelTap  = channelTap .* rayLaw;
%    rng('shuffle');
end

% Convolution !
sigChan		  = conv(sigPrefix,channelTap);

% --- Calculate average power 
puissIn		  = 1/length(sigChan)*sum(abs(sigChan).^2);


% ---------------------------------------------------- 
%% --- Prepare additive white gaussian noise 
% ----------------------------------------------------
% --- We have a SNR, not Eb/No 
% In non full alloc, that's a problem. Let's evaluate spectral efficiency and do 
% some shenaniggans to have something related to what bits see in terms of information theory
%FIXME Not debugged channel power: maybe adding  "/ sqrt(sum(abs(channelTap).^2)) " ?
ebNo =10*log10( 10.^(snrChannel/10) * (sizeFFT + sizePrefix) / (log2(sizeConstellation) * nbSubcarriers)); 
% --- Generates a white additive circular gaussian noise 
whiteNoise=sqrt(puissIn)/sqrt(2)*10^(-ebNo/20)*(randn(1,length(sigChan))+1i*randn(1,length(sigChan)));	
% --- Adding AWGN 
sigRx	  = sigChan + whiteNoise;

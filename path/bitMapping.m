% --- bitMapping.m
% ---
% Description
%		Match bit sequence with adequate digital constellation.
%		Gray Coded is applied.
% ---
% Syntax
% 		[seqMapped] = bitMapping(bitSequence,modForm);
% 		seqMapped	: Mapped Constellation
% 		bitSequence	: Sequence of bits to be mapped
% 		modForm		: Size of constellation (bit per symbol)
% ---
% v 1.0 - Jan. 2013 - Robin Gerzaguet.

function [seqMapped] = bitMapping(bitSequence,modForm)
    % --- Check That Mapping does not left bits outside
        extrabits = mod( length(bitSequence),(log2(modForm)));
        if extrabits
            display('illegal length - bits left over');
            bitSequence = bitSequence(1:end-extrabits);
        end
    map4 = [0 ...
        1];
    
    map16 =[0 ...
        1 ...
        3 ...
        2];
    
    map64 = bitxor(0:8-1, floor((0:8-1)/2));
    bin16 = [ 2 1];
    bin64 = [ 4 2 1];
    scalingFactor = sqrt(2/3*(modForm-1));
    % --- Mapping Application
    switch modForm
        case 2
            % --- BPSK
            seqMapped = -1 + 2*bitSequence;
            scalingFactor = 1;
        case 4
            % --- QPSK
            temp = reshape(bitSequence,2,length(bitSequence)/2) +1; % for matlab
            temp = map4(temp);
            if length(bitSequence) == 2
                temp = reshape(temp, 2, []);
            end
            % --- Complex Signal
            seqMapped = temp(1,:) + 1i*temp(2,:);
            % --- Translation
            seqMapped = 2*(seqMapped -(map4(2)-map4(1))/2*(1+1i));
        case 16
            % --- 16 QAM
            % --- I-Path Mapping
            tempI = bitSequence(1:2:end);
            tempI = reshape(tempI,2,length(tempI)/2);
            tempI = bin16 * tempI + 1;
            tempI = map16(tempI);
            % --- Q-Path Mapping
            tempQ = bitSequence(2:2:end);
            tempQ = reshape(tempQ,2,length(tempQ)/2);
            tempQ = bin16 * tempQ + 1;
            tempQ = map16(tempQ);
            % --- Complex Signal
            seqMapped = tempI  + 1i*tempQ;
            % --- Translation
            seqMapped = 2*(seqMapped -1.5*(1+1i));
        case 64
            % --- 64 QAM
            % --- I-Path Mapping
            tempI = bitSequence(1:2:end);
            tempI = reshape(tempI,3,length(tempI)/3);
            tempI = bin64 * tempI ; % constellation mapping w/o coding
            tempI = map64(tempI+1); % +1 to have elements of array
            % --- Q-Path Mapping
            tempQ = bitSequence(2:2:end);
            tempQ = reshape(tempQ,3,length(tempQ)/3);
            tempQ = bin64 * tempQ + 1;
            tempQ = map64(tempQ);
            % --- Complex Signal
            seqMapped = tempI  + 1i*tempQ;
            % --- Translation
            seqMapped = 2*(seqMapped -3.5*(1+1i));
        otherwise
            error('invalid mod modForm');
    end
    % --- Normalisation
    seqMapped = seqMapped/scalingFactor;
end

% --- carrierConstellation .m 
% --- 
% Description 
%    Display Constellation of an OFDM signal before or after egalisation for all carrier data 
%	  The input is a  matrix of complex mapped symbols Nc x Ns where Nc is the number of carrier and Ns the number of symbols
% --- 
% Syntax 
%    carrierConstellation(matrixSymbol)
% --- 
% v 1.0 - Robin Gerzaguet.



function carrierConstellation(matrixSymbol)

matrixSymbol = matrixSymbol.';
	% ---------------------------------------------------- 
	% --- Prepare figure 
	% ---------------------------------------------------- 
	nF	  = 1;
	nrows = 4;
	ncols = 4;
	% --- Rearrange carrier from 1 -> end  [ex : 9 10 .. 16 1 .. 9]
%	matrixSymbol  = [ matrixSymbol(1+end/2:end,: ); matrixSymbol(1:end/2,:)];
%	boundAxes	  = zeros(1,4);
	for ii = 1:size(matrixSymbol, 1)
		if nrows*ncols-mod(-ii, nrows*ncols) == 1
			figure('name',['Carrier Constellation : Figure ' num2str(nF)]);
			nF	  = nF +1;
		end
		h(ii) = subplot(nrows, ncols, nrows*ncols-mod(-ii, nrows*ncols));
		plot(real(matrixSymbol(ii,:)),imag(matrixSymbol(ii,:)),'marker','x','lineStyle','none','lineWidth',3);
		grid minor;
		xlabel('Real Part');
		ylabel('Imag Part');
		title(['Carrier Constellation : Index ' num2str(ii)]);
		% --- Getting axes bound for same display
		axis([-2 2 -2 2]);
%		k = axis;
%		boundAxes(1,[1 3]) = min(boundAxes(1,[1 3]),k(1,[1 3]));
%		boundAxes(1,[2 4]) = max(boundAxes(1,[2 4]),k(1,[2 4]));
	end
%	linkaxes(h);
	% --- Apply same display feature
%	axis(boundAxes);
end

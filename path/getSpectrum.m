% --- getSpectrum.m
% --- 
% Description
%		Display Spectrum analysis of entries signals
% ---
% Syntax
% 		fig = getSpectrum(fe,sig1,...,sigN);
% 		fe 				: Sample Frequency
%		sig1,...sigN 	: signal to be analyzed
%		fig 			: Figure
% ---
% v 1.0 - Apr. 2013 - Robin Gerzaguet.

function ah = getSpectrum(varargin)
	% --- Check that at least 2 parameters are present
	if nargin < 1
		error('simRF:getSpectrum','2 parameters must be set');
	end
	% --- Extract and check frequency
	fe = varargin{1};
	if length(fe)>1
		error('simRF:getSpectrum','1 parameter must be the frequency');
	end
	% --- Sided System
	freqNeg = 1;
	% If 0 : 0 : Fm
	%	 1 : -Fm/2 : Fm/2 
	% --- Get number of signals
	nbToPlot = nargin - 1 ;
	col = hsv(nbToPlot);
	% --- Prepare figure
	ah = figure('name','Spectrum analysis');
	str = cell(1,nbToPlot);
	% --- Iterative display
	for i = 1 : 1 : nbToPlot
		% --- Get Name
		str{i}	= inputname(i+1);
		% --- Signal
		x		= varargin{i+1};
		x		= x(:);
		nFFT 	= length(x);
		% --- Frequency Vector
		k 		= fe*[0:nFFT-1]/nFFT;
		% --- Apodisation (Hamming Window)
		apodHamm= hamming(length(x));
		xHam 	= x.*apodHamm;
		% --- TFD
		SHamm	= fft(xHam)/(sqrt(sum(apodHamm.^2)))/sqrt(fe);
		% --- Adding Plot
		if ~freqNeg
			plot(k,db10(SHamm.^2),'color',col(i,:),'LineWidth',3);
			semilogx(k,db10(abs(SHamm)),'color',col(i,:),'LineWidth',3);
			hold on;
		else
			spectr = 20*log10(abs(SHamm));
%			spectr = (abs(SHamm));
			plot([-k(end/2:-1:1) k(1:end/2)],[spectr(end/2+1:end)' spectr(1:end/2)'],'color',col(i,:),'LineWidth',3);
			hold on
		end
	end
	% --- Adding legend
	grid minor;
	legend(str);
	% --- Titles
	xlabel('Frequency (Hz)');
	ylabel('Power Spectral Density (dBc/Hz)');
	title('Spectrum Analysis');
end

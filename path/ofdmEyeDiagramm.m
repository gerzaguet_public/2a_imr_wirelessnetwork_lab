% --- ofdmEyeDiagramm.m 
% --- 
% Description 
%    Display eye diagram for each OFDM carrier  ; 
% --- 
% Syntax 
%    ofdmEyeDiagramm(A,masquePorteuse)
%				A				  : Frequency domain OFDM matrix (input of IFFT)
%				masquePorteuse	  : mask for enabled carriers
% --- 
% v 1.0 - Robin Gerzaguet.


function ofdmEyeDiagramm(X,masquePorteuse,flag)
	% --- Flag for standalone -- multicarrier
	if nargin == 2
		flag = 0;
	end
	if length(masquePorteuse) == 1
		% --- Unique enable carrier 
		overSamp  = 128;
		xData	  = X(masquePorteuse,:);
		% --- Number of symbol view 
		numSymb     = size(X,2);
		% --- Reshaping for one trajectory
		abscis	= linspace(0,1,overSamp);
		EyeP	= repmat(xData.',1,overSamp) .*  repmat(exp( 2*1i*pi* (masquePorteuse-1)* abscis),numSymb,1) ;
		% --- Display
		if flag == 0
			% --- Standalone Figure 
			figure('name',['Eye Diagramm for carrier ' ,num2str(masquePorteuse)]);
			a	  = 1;
			b	  = 2;
			cnt	  = 1;
		else 
			% --- Part of subplot
			if mod(masquePorteuse-1,2) == 0
				figure('name','Eye diagramm for carriers');
				cnt	  = 1;
			else
				cnt = 3;
			end
			a	= 2;
			b	= 2;
		end
		% --- Plot Eye diagramm
		subplot(a,b,cnt)
		plot(real(EyeP.'));
		subplot(a,b,cnt+1)
		plot(imag(EyeP.'));
		% --- Adding title, and some stuff 
		subplot(a,b,cnt)
		title(['Eye Diagramm [real part] for carrier ' num2str(masquePorteuse)]);		
		subplot(a,b,cnt+1)
		title(['Eye Diagramm [imag part] for carrier ' num2str(masquePorteuse)]);		
	else
		figure('name','OFDM Eye Diagram');
		% --- Full OFDM spectum 
		for j = masquePorteuse
			ofdmEyeDiagramm(X,j,1);
		end
	end
end

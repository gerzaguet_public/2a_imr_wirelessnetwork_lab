% --- carrierConstellation .m
% ---
% Description
%    Display Constellation of an OFDM signal before or after egalisation for all carrier data
%	  The input is a  matrix of complex mapped symbols Nc x Ns where Nc is the number of carrier and Ns the number of symbols
% ---
% Syntax
%    carrierConstellation(matrixSymbol,H)
% ---
% v 1.0 - Robin Gerzaguet.



function effectCarrierConstellation(matrixSymbol,H,subcarriers)


nrows = 4;
ncols = 4;
% We will plot 16 elements,
% Finding associated subcarrier index
nbToPlot    = nrows * ncols;
nFFT    = size(matrixSymbol,1);
spaceIn = floor( nFFT / nbToPlot);
elemn   = (1:spaceIn:nFFT);
elemn   = elemn(1:nbToPlot);

% ----------------------------------------------------
% --- Prepare channel response figure
% ----------------------------------------------------
% --- Prepare figure
figure('Name','Channel response');
% --- For Magnitude
subplot(2,1,1)
grid minor;
hold on
% ----------------------------------------------------
% --- Display Frequency
% ----------------------------------------------------
sFFT  = 2^nextpow2(nFFT);
absci	= (-0.5+(0:sFFT-1)/sFFT);
plot(absci,abs(fftshift(fft(H.',sFFT))),'lineWidth',3,'LineStyle','-','color','blue');
xlabel('Normalized frequency');
ylabel(' | H |');
title('Frequency Response of Channel');
zz	= axis;
zz	= [-0.5 0.5 0 zz(4)];
axis(zz);
plot([-0.5 0.5],[0 0],'lineWidth',2,'LineStyle','-','color','black');
% --- Adding Carrier frequencies
vectC	= [0 : 1 / sFFT : 0.5 - 1/sFFT -0.5:1/sFFT:-1/sFFT];
xpos    = vectC(subcarriers(elemn));
for i = 1 : 1 : length(xpos)
    plot([xpos(i) xpos(i)],[zz(3) zz(4)],'lineWidth',1,'LineStyle','--','color','black');
    text(xpos(i),zz(4),['P : ' num2str(elemn(i))]);
end

% ----------------------------------------------------
% --- Display Phases
% ----------------------------------------------------
subplot(2,1,2);
grid minor;
hold on
% --- Frequency response
plot(absci,angle(fftshift(fft(H.',sFFT))),'lineWidth',3,'LineStyle','-','color','red');
xlabel('Normalized frequency');
ylabel(' Phase ');
title('Phase  Response of Channel');
% --- Adding Carrier frequencies
vectC	= -0.5 : 1 / sFFT : 0.5 - 1/sFFT;
vectC = vectC(elemn);
zz		= axis;
for i = 1 : 1 : length(xpos)
    plot([xpos(i) xpos(i)],[zz(3) zz(4)],'lineWidth',1,'LineStyle','--','color','black');
    text(xpos(i),zz(4),['P : ' num2str(elemn(i))]);
end
zz  = axis;
zz	= [-0.5 0.5 zz(3) zz(4)];
axis(zz);
plot([-0.5 0.5],[0 0],'lineWidth',2,'LineStyle','-','color','black');



% ----------------------------------------------------
% --- Prepare carrier constellation figure
% ----------------------------------------------------
figure('name','Carrier constellation');
% Plotting subcarrier carrier
for ind = 1:1:length(elemn)
    % X/Y subplot coor
    subplot(nrows,ncols,ind),
    plot(real(matrixSymbol(elemn(ind),:)),imag(matrixSymbol(elemn(ind),:)),'marker','x','lineStyle','none','lineWidth',3);
    grid minor;
    xlabel('Real Part');
    ylabel('Imag Part');
    title(['Subcarrier Index ' num2str(elemn(ind))]);
end
end

% --- rx_stage.m 
% --- 
% Description 
%	  Script for OFDM  decoding 
% --- 
% Syntax 
%     rx_stage
%
% --- 
% v 1.0 - Robin Gerzaguet.




% ---------------------------------------------------- 
%% --- OFDM decoding  
% ---------------------------------------------------- 
% --- Prepare signal 
% In the end, we shall have a matrix of decoded QAM symbols 
qamRx	    = zeros(nbSubcarriers,nbOFDMSymbols);			% --- Before equalisation 
% ---------------------------------------------------- 
% ---------------------------------------------------- 
% !!!!!!!  TODO !!!!!!!!!!!!!
% Create the OFDM demodulator
% ----------------------------------------------------
%% Beginning of "To Be Completed (TBC)" area 









%% END OF TBC AREA
% ---------------------------------------------------- 
%% --- Channel equalisation 
% ---------------------------------------------------- 
% CSI based ZF equalizer
chest   = repmat( mean(qamRx ./ qamAll(subcarriers,:),2),1,nbOFDMSymbols);
% Channel inversion
qamRxEq   = qamRx ./ chest;

% ---------------------------------------------------- 
%% --- Binary decoding  
% ---------------------------------------------------- 
% --- From Matrix constellation, deduce bit sequence 
bitRx	  = bitUnMapping(qamRxEq(:),sizeConstellation);
% --- Calculate performance indicators 
ber		  = sum(xor(bitRx,bitSeq(1:length(bitRx)))) / length(bitRx);
fprintf('OFDM simulation \n BER = %f \n',ber);
if snrChannel > 80 && ber ~=0
    fprintf('OFDM demodulator not functionnal\n');
end
viewConstellation(qamRx);
